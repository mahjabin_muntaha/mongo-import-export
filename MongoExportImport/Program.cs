﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Diagnostics;
using System.IO;

namespace MongoExportImport
{
    public class Program
    {
        public static string MongoToolsRelativePath => @"";
        static void Main(string[] args)
        {
            string relativePath = @"C:\Treaty\_shahadath_Treaty\_shahadath_Treaty";
            import(relativePath);

        }
        static void export(string database, string relativePath)
        {
            var connectionString = "mongodb://localhost";
            var client = new MongoClient(connectionString);
            var db = client.GetDatabase(database);

            foreach (var item in db.ListCollectionsAsync().Result.ToListAsync<BsonDocument>().Result)
            {
                var name = item["name"].AsString;
                using (Process process = new Process())
                {
                    process.StartInfo.FileName = "CMD.exe";
                    process.StartInfo.WorkingDirectory = @"C:\Program Files\MongoDB\Server\4.0\bin";
                    process.StartInfo.Arguments = string.Format(@$"/C mongoexport -d {database} -c {name} --out {relativePath}/{database}/{name}.json");
                    process.Start();
                    //process.StartInfo.ArgumentList.Add(string.Format("git push {0} {1}", remoteName, branch));
                    process.WaitForExit();
                }
            }


            Console.WriteLine("Hello World!");
        }


        static void import(string relativePath)
        {
            var path = Directory.GetFiles(relativePath);
            foreach (var filePath in Directory.GetFiles(relativePath))
            {
                using (Process process = new Process())
                {
                    var fileName = Path.GetFileName(filePath);
                    var databaseName = Path.GetFileName(Path.GetDirectoryName(filePath));
                    process.StartInfo.FileName = "CMD.exe";
                    process.StartInfo.WorkingDirectory = MongoToolsRelativePath;
                    process.StartInfo.Arguments = string.Format(@$"/C mongoimport -d {databaseName} -c {Path.GetFileNameWithoutExtension(fileName)} --file {filePath}");
                    process.Start();
                    //process.StartInfo.ArgumentList.Add(string.Format("git push {0} {1}", remoteName, branch));
                    process.WaitForExit();
                }
            }

        }
    }
}
